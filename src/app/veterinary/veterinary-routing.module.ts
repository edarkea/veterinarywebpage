import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { VeterinaryComponent } from './veterinary.component';
import { NotFoundComponent } from '../pages/miscellaneous/not-found/not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [{
  path: '',
  component: VeterinaryComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class VeterinaryRoutingModule {
}
