import { NgModule } from '@angular/core';
import { VeterinaryComponent } from './veterinary.component';
import { VeterinaryRoutingModule } from './veterinary-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { NbMenuModule } from '@nebular/theme';
import { DashboardModule } from '../pages/dashboard/dashboard.module';
import { ECommerceModule } from '../pages/e-commerce/e-commerce.module';
import { MiscellaneousModule } from '../pages/miscellaneous/miscellaneous.module';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [VeterinaryComponent, DashboardComponent],
  imports: [
    VeterinaryRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
  ]
})
export class VeterinaryModule { }
