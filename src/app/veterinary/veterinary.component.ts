import { Component } from '@angular/core';
import { MENU_ITEMS } from './veterinary-menu';
@Component({
  selector: 'ngx-veterinary',
  styleUrls: ['./veterinary.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
  
})
export class VeterinaryComponent{

  menu = MENU_ITEMS;

}
